Part of the microfrontend reference architecture described in https://gitlab.com/javier-sedano/ufe5

Read that documentation.

This is an HTTP reverse proxy used to route URLs.
