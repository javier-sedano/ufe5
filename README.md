Reference architecture for a microfrontend architecture with SSR using Nuxt3 and Vue3.

To study tihs project, [Visual Study Code containerized development environment](https://code.visualstudio.com/docs/devcontainers/containers) is strongly suggested. In theory, everything can be run natively as well, but the containerized environment is the one I use, so the [quick instructions](#quick-instructions) assume it.

- [Requirements](#requirements)
- [Quick instructions](#quick-instructions)
- [Architecture](#architecture)
   * [Design principles](#design-principles)
   * [Folder structure](#folder-structure)
      + [Submodule vs project](#submodule-vs-project)
   * [HTTP router](#http-router)
   * [DRY](#dry)
      + [Shared libraries](#shared-libraries)
      + [Component libraries](#component-libraries)
      + [dev-utils](#dev-utils)
   * [Common login](#common-login)
   * [Autoversioning](#autoversioning)
      + [What autoversioning is not](#what-autoversioning-is-not)
   * [Autoupgrading](#autoupgrading)
   * [Share state](#share-state)
      + [Share state through local storage example: shared library](#share-state-through-local-storage-example-shared-library)
      + [Share state through local storage example: component library](#share-state-through-local-storage-example-component-library)
      + [Share state through local storage example: pinia store backed by localStorage](#share-state-through-local-storage-example-pinia-store-backed-by-localstorage)
      + [Share state through URL example](#share-state-through-url-example)
   * [app-contact](#app-contact)
   * [Caveats (and solutions)](#caveats-and-solutions)
- [Development modes](#development-modes)
   * [Clone everything and run everything](#clone-everything-and-run-everything)
   * [Clone everything but run partially](#clone-everything-but-run-partially)
   * [Clone standalone](#clone-standalone)
   * [Yarn link](#yarn-link)
      + [Introduction to `yarn link`](#introduction-to-yarn-link)
      + [How to use it in the root project](#how-to-use-it-in-the-root-project)
      + [How to use it in standalone clones](#how-to-use-it-in-standalone-clones)
- [FAQ](#faq)
- [TODO](#todo)

# Requirements

* Visual Studio Code (1.83.1 tested)
    * Extensions:
        * Dev Containers (0.315.1 tested) (strongly suggested)
            * Node 18.18.1 is included in the container definition, but if you are not using the containerized development environment, you need it.
* Rancher Desktop (1.10.0 tested)
    * Probably Docker Desktop can be used as well, but not tested.

# Quick instructions

The different [development modes](#development-modes) will be explained later, after the architecture, but for a quick start you can use these quick instructions:

* Clone the project in-container:
    * Open Visual Studio Code
    * Use the command palette (F1, CTRL-SIFHT-P, COMMAND-SHIFT-P) and select `Dev Containers: Clone Repository in Named Container Volume...`.
    * Type the URL to this repo `git@gitlab.com:javier-sedano/ufe5.git` or `https://gitlab.com/javier-sedano/ufe5.git`.
    * When asked about the volume, choose the `vsc-remote-containers` volume or your volume of choice.
    * When asked about the path, choose the default or your path of choice.
    * Alternatively, you can theoretically clone using git and open the project without dockerization, but I have not tested it.
* Initialize with `yarn install && yarn initAll`. This will autommatically:
    * Install dependencies in the root project (`yarn install`).
    * Initialize submodules (`yarn init:submodules`).
        * Later you can use `yarn pull:submodules` and/or `yarn pull` to pull everything with a single command.
    * Checkout `main` in all submodules (`yarn init:checkoutMain`).
        * Later you can change the branches, but initially it is easier if everything is aligned to the HEAD of main.
    * Initialize the dependencies (`yarn init:dependencies`).
* Run all of the components (applications and libraries) in development mode with `yarn dev`.
    * Each of the projects is using hot-rebuild mode (watch mode, dev mode or whatever you want to call it), but they are not linked together. This is enough for this quickstart, but read [Yarn link](#yarn-link) below for a more suitable development mode.
* Browse [http://localhost:4100](http://localhost:4100).

Find different [development modes](#development-modes) later in this documentation.

# Architecture

## Design principles

* Allow each microfrontend to be developed independently of the others by different teams.
    * Separated lifecycles.
    * Separated deployments, and even different deployment strategies (CD, release train,...).
    * Separated Git repositories: different permissions, different rules, etc.
    * Separated quality rules: test coverage, Sonarqube configuration, security constraints, pipeline stages,...
* Reuse common code. DRY.
    * Shared libraries.
    * Shared visual components.
    * Shared pipeline steps.
    * ...
* Vue3.
* SSR using Nuxt.

## Folder structure

Each of the microfrontends and libraries is a different Git repository. All of them are brought to this project as submodules. Also, each of them is a separate package.json project.

With this approach we can achieve two things at the same time:

* Each project can be a separate and independent project, with separated lifecycle, separated publication/deployment, etc
* All of them can be developed together (however, this is not the only posibility, see below).

A lot of NPM Scripts are provided in the root `package.json`, to ease the management of this structure. In particular, the following topics are important:

* `yarn pull` will do a `git pull` in the root repository and in all of the submodules, to bring them all to the HEAD.
    * WARNING: watch the output of the command, because if you made modifications in parallel, which is likely if you are doing developments in a library and an application at the same time, the `git pull` may fail, and you may need to stash-pull-unstash to solve it.

* The CI pipeline of each of the submodules will automatically update the pointers to the HEAD of the main branch of the submodules. You just need to `git pull` everything after the pipelines of main of each submodule (see the previous topic).
    * NOTICE: If more than 1 instance of this update are running in parallel (because two submodules are running the pipeline in parallel), this job in the pipeline may fail (depending on when the collission happens). So this job is configured to retry up to 2 times (maximum allowed by GitLab CI). This does not 100% avoid the situation, only turns it less likely; if this autoupdate causes this fail, probably you should retry the job in GitLab before investigating further.

* No NPM workspace is used. Each project is completely independend. It means that the dependencies are managed as isolated projects. See the `yarn link` feature below to extremely ease the interaction during the development.

### Submodule vs project

There may not be a 1-to-1 relationship between submodules and projects.

For example, the `infra-router/` folder is a project (to isolate dependencies and needs) but is not a submodule (because it only makes sense as part of the all-together architecture).

Something similar could be made (but we have not done it here) with the `component-library-vue3/` submodule. Currently `component-library-vue3/` only contains one project, but it may itself be a monorepo containing several projects (one for each component), maybe using NPM workspaces and/or lerna.

## HTTP router

The core of the architecture is an HTTP router (a reverse proxy), which routes each path to a different application.

Each of the applications runs in a different host/port, and the router shows an unified application to the user:

* Incomming port 4100
    * /account -> localhost:4104 (app-account)
    * /contact -> localhost:4102 (app-contact)
    * /products -> localhost:4103 (app-products)
    * /* -> localhost:4101 (app-core)

The router for the development environment is hosted at `infra-router/`. It must mimic the configuration of the production environment. For the development environment it uses `express` and `http-proxy`. For the producton environment, Apache, NGIX or a similar solution can be used.

The router contains a catch-all route as a fall back, to allow 404. It does not mean that 404 mustn't be handled by each of the microfrontends, because 404 can also happen inside each of the routes.

## DRY

### Shared libraries

Shared libraries are developed independently and published to `npmjs.com`.

You can see `lib-login/` as example.

### Component libraries

A Vue3 component library is provided at `component-library-vue3/`.

A design system with Atoms, Molecules and Organisms can be provided, but since this is not the objective of this architecture demonstration, no effort has been put on it.

### dev-utils

Common development utils are provided at `dev-utils/`.

Right now it only contains definition of common jobs for GitLab-CI, but other options include: development scripts, linter rules,...

The folder is an NPM project published to `npmjs.com`... but this publication is mostly useless, because the shared GitLab-CI jobs must be consumed directly from Git, not from `npmjs.com`. Well... it is kept like this for further growth.

## Common login

Login is based on an OIDC authentication to a cloud IdP managed by me. For testing, you can use the user `a` password `a`.

The ID Token and Access Token are stored in the Local Storage (actually, this is the default configuration of the `oidc-client-ts` library that we are using), so it is available for all of the microfrontends in the domain... so when you login in one of the applications (the `app-account` application), the tokens can be used by all of them. In particular, the shared graphical component ClvMenu uses it.

As an alternative, a clasical session-cookie-based authentication could be used. In the client side, it would be easy, because the cookie can be seen by every path in the same domain (if the cookie is properly configured). However, in the server side sharing of the cookie between the different microfrontends depends on the backend technology that is being used, and may be hard.
 
## Autoversioning

During the CI pipeline, each project autoversions itself. After the project is built and published/deployed, it increments the minor version, letting the HEAD of the project ready for the next development.

It means that at any given moment the `version` field of the `package.json` of each project does not contain the CURRENT version, but the **NEXT** version.

Does it mean that the next version must be a minor version increment? Not at all. This is only the increment that the autoversioning is bumping. But if your development is either a patch or a major, you can just change the `version` accordingly, and it will just be used during the CI/CD pipeline.

You will find `yarn pull` useful to deal with this autoversioning.

### What autoversioning is not

Notice that the autoversioning has nothing to do with the management of the versions of the dependencies. For example, imagine the following situation:

* The last published version of `lib-login` is 3.14.16 and its `package.json` has `version` field 3.15.0 .
* `app-account` depends on `lib-login@3.14.16` .

During the pipeline of `lib-login`, it will publish version 3.15.0, and will modify the field `version` of the `package.json` to 3.16.0 .

However, `app-account` still depends on `lib-login@3.14.16`; if you want to use the newly published version of `lib-login` you must modify `app-account` to depend on `lib-login@3.15.0`.

But... wait a second... if I read `app-account/package.json` the dependency is `^1.0.0`... why it is not using the latest version of `lib-login`? Because the real version is stored in `yarn.lock` (I hope you ar familiar with this; other wise, read https://classic.yarnpkg.com/lang/en/docs/yarn-lock/).

How do we solve this? See [Autoupgrading](#autoupgrading).

## Autoupgrading

This capability does the following: when we have A depending on B, and a new version of B is published, A is automatically upgraded to use the latest version of B.

How?

The dependency from A to B is using B@^1.0.0, and is keeping the especific version in `yarn.lock`. Then, when a pipeline of B is trigered in the branch `main`, all the dependent projects are cloned, upgraded (using `yarn upgrade`), pushed, and MRs are created. Optionally, the MRs are automatically merged (see below).

If the automerge is enabled (by default it is), after pushing some changes to the branch `main` of one of the libraries (`lib-login` or `component-library-vue3`), you just need to wait for a few minutes and do a `yarn pull` in the root project, and everything will be aligned again automatically.

If the automerge is not enabled (removing `-o merge_request.merge_when_pipeline_succeeds` from `/dev-utils/gitlab-ci/.gitlab-ci-updateYarnLock.yml:20`), everything until the creation of the MRs is automatic, but you must review and merge the MRs yourself.

Is it better to allow the automerge or not? It depends. It is a risk that you need to assess and decide whether to take or not. It highly eases the work... but... if there is an error, the branch `main` of the microfrontends are built and deployed. How can you mitigate this risk? Several ideas:

* If you have a good automatic test coverage, a potential error would be detected. In this PoC we are not making these tests, because it is not the goal of the architecture, but in a real project, you are doing a lot of automated tests, aren't you?

* In this PoC we are using a feature-branch git strategy. But if you are using gitflow or similar, this autoupgrading must modify the `develop` branches (instead of `main`). Hopefully you are deploying all the `develop` branches of all the microfrontends to an integration environment where you can test before merging to `main` and deploy to production.

* If you are deploying `main` into a preproduction environment (which you should) again you are testing there before going to production, aren't you?

However, even with those mitigations... when the whole project is large (with many microfrontends) and you are modifying a part deep down in the dependency tree, the automerge may cause a waterfall of upgrades, making `main` non-coherent for some time untill everything stabilices itself. Do not overstimate the help of the automation... it eases many things, but it is still a complex situation. For example... with this reference project: when `lib-login` is modified, it will trigger MR/merge/build of `app-account` and `component-library-vue3`... which in turn will trigger MR/merge/build of `app-account` (again!), `app-contacts`, `app-core` and `app-products`. As said: asses the risk. Anyway, you can always disable the automerge and review the MRs yourself before you merge... but do not let this fool you: merging manually does not make it easier. Consider any change in deep dependencies with proper care, tests, backward compatibility+deprecation, etc as you would do for any library.

NOTICE: If more than 1 instance of this upgrade are running in parallel (because two submodules are running the pipeline in parallel), this job in the pipeline may fail (depending on when the collission happens). So this job is configured to retry up to 2 times (maximum allowed by GitLab CI). This does not 100% avoid the situation, only turns it less likely; if this autoupgrade causes this fail, probably you should retry the job in GitLab before investigating further.

You will find `yarn pull` useful to deal with this autoupgrading.

## Share state

As said, each of the microfrontends is a different application, they do not share state. If two of the microfrontends need to share a lot of state... why are they two separe microfrontends, to start with?

However if some state needs to be shared, the following solutions can be used:
* Share the state through the server (backend).
* Share the state through Web Storage:
    * See some examples below, in their specific sections. In these examples we will use Local Storage, but Session Storage could have been used as well.
* Share the state through the URL.
    * See one example below, in their specific section.

Alternatively, cookies may be used to share the state. It has some benefits an drawbacks:
* Cookies are shared between the client and the server. This has nothing to do with sharing state between applications, but it may have worth by itself and may have its own challenges.
* Cookies are sent to the server and back to the client. So it uses bandwidth.
* The maximum size for cookies is lower than for Web Storage.

### Share state through Local Storage example: shared library

`lib-login` uses oidc-client-ts, which in turn stores [by default] the state of the authentication in the Local Storage.

### Share state through Local Storage example: component library

`ClvSearch` in `component-library-vue3` stores its own state in the Local Storage, so when it is used in diferent pages, it behaves as a continuous.

Do not confuse this example with the example below about using the URL: [Share state through URL example](#share-state-through-url-example). `ClvSearch` triggers such URL as well but there it only behaves as trigger, while here it is the core of the example.

### Share state through Local Storage example: pinia store backed by localStorage

`app-contact`, `app-core` and `app-products` use a pinia store which is saved into the localStorage using https://prazdevs.github.io/pinia-plugin-persistedstate/ .

The store itself is a library in `store-library-pinia`, to make sure that all of them use the same store. `app-core` and `app-products` use this library. `app-contact` does not use it, though; this is done to demonstrate that interoperability between different versions (having different store properties) is possible (for example, an application using an old version of the store library or not using the library at all, like in this case).

Does it mean that we must use this solution for every store? NOT AT ALL! Only for the state that needs to be shared between applications. Read the documentation of `pinia-plugin-persist` and look for `persis: true` in our example.

WARNING: the `store-library-pinia` is published in both source and compiled form, but it is imported in source form in the applications. Why? Because when I tried to use the compiled form, it works for `app-products` (Vue3) but not for `app-core` (Nuxt3).

### Share state through URL example

This is specially useful, because if you have two different microfrontends, they are in different URLs. If you want to share some state between them, probably is because you have some stateful navigation... so using the URL to communicate the state is probably a good idea anyway (if the state is small) to allow deep-links, bookmarks, SEO,...

You can see an example of this idea in the search feature of this project. The search results is implemented in app-core, and linked from app-core itself, app-contact and app-products; ClvSearch is the reusable component that does the navigation.

Do not confuse this example with the sharing of the state through the Local Storage above: [Share state through local storage example: component library](#share-state-through-local-storage-example-component-library). There, `ClsSearch` is the core of the example; here, it is only the trigger of the navigation.

## app-contact

`app-contact` is used intentionally with different versions of some things, to emulate a project that has fall behind in terms of updates.

So far, the following things are intentionally out of date:

* `total.store` is not using the library `store-library-pinia`, so it is using it's own implementation that has intentionally fall behind and does not use have all the fields of the store.
* Nuxt3 version is older than the version usd by `app-core`.

## Caveats (and solutions)

* As with any micro-something architecture, there are trade-offs: some things are easier, some things are harder. Microfrontends is not a silver bullet that will solve all of your problems. There is a lot of documentation about this topic in the network; this project is not meant to be the next article about it, so search the web for it. But I didn't want not to mention it, in case someone reaches here without further documentation.

* Each of the the microfrontends is a different application, so any navigation between them causes a full HTTP navigation, instead of an SPA navigation; this implies two things: (i) an "ugly" reload effect to the user. And (ii) a lost of the state of the application (see above for ideas). Hopefully you do not have many cross-microfrontend navigations where this is a problem... otherwise... have you considered that maybe those shouldn't be separate microfrontends?

# Development modes

Depending on your needs, different development modes can be used.

TL;DR :
* To run this example, use [Clone everything and run everything](#clone-everything-and-run-everything)
* For a real-world project, use [Clone everything but run partially](#clone-everything-but-run-partially)

## Clone everything and run everything

The first option, which is the option described in the [Quick instructions](#quick-instructions) above, is summarized as:

* Clone the root Git repository.
* Initialize the submodules.
* Run all of the applications using the `yarn dev` script. It runs the HTTP router and all the microfrontends.

The main benefit of this approach is that it is very easy and straightforward. However, it has two main drawbacks:

* If the number of microfrontends or modules is large, the resources needed for this approach may be too high. See the [alternative modes below](#clone-everything-but-run-partially).
* Each application and library and everything runs with hot-swap... but they are not linked together, so changing a library does not have an effect in the application. This can be solved with the [Yarn link](#yarn-link) below.

This repository contains scripts to use this mode by default.

## Clone everything but run partially

This method is slightly more complex, but more powerfull in the long term (especially when there are many projects).

* Clone the root Git repository.
* Initialize the submodules.
* Modify the configuration of the `infra-router` proxy to route:
    * The microfrontend you are interested in, to your localhost (same as default).
    * The rest of them, to your central development integration environment (1).
* Run only `yarn dev` in both `infra-router` and your microfrontend.

(1) Actually if you are going to use this approach frequently, maybe the proxy in `infra-router` should be configured by default to route to the central development integration environment, and change only the one you want to route to localhost. Here we are not using this approach because it is only a demonstrator and we do not have such "central development integration environment".

These are the main drawbacks:

* You need a central development integration environment... but you have it anyway, don't you? ;-)
* The central development integration environment frontend is probably using an equally central development integration environment backend. So, in order to maintaing backend data consistency, you need to configure your local frontend development to use that very same backend. Probably this is your default configuration anyway... but when you need to change it (for example because you are debugging an integration between frontend and backend), things start to be more difficult. This problem does not show up in this example because we are not developing a full application with backend.

The [Yarn link](#yarn-link) configuration below may be still needed and is doable.

This repository contains scripts to run by default the mode [Clone everything and run everything](#clone-everything-and-run-everything) but it can be easily adapted. Hints:

* The following scripts are intended to operate on all the projects, so do not use them. Either do such operations manually or adapt the scripts, to do it only for the projects you are interested in.
    * `yarn initAll` and the related scripts `init:*`
    * `yarn pull`
    * `yarn linkAll` and `unlinkAll`
    * `yarn dev` (but you can use each script `dev:*` separatedly).

* Configure and commit the `infra-router` to use your central development integration environment by default (so each developer will need to mofidy it to point to his local environment only for his own application).

## Clone standalone

In this mode, you can clone only the repository you are interested in, and run `yarn dev` only in this project. The only thing to remind is that for browsing you must use the port of the microfrontend, not the port of the proxy; for example for `app-core` you must use [http://localhost:4101](http://localhost:4101) (1).

(1): actually, even if you are using the modes [Clone everything and run everything](#clone-everything-and-run-everything) or [Clone everything but run partially](#clone-everything-but-run-partially), the project-specific URLs such as [http://localhost:4101](http://localhost:4101) are available as well, because all the ports are mapped.

The main benefit is that it uses less resources.

The main drawbacks are:

* The URLs that are pointing to other microfrontends, obviously will not work. If you have frequent navigations between them, it will be hard to develop. But if this is the case... why are they different microfrontends, to start with?
* The code of the application must not depend on the URL it is deployed to... but this is something you are already doing anyway, right? (otherwise, how are you going to deploy to production?)

## Yarn link

### Introduction to `yarn link`

If you are not familiar with `yarn link`... it allows you to create a symbolic link between two packages (the dependent A and the dependency B) so you can develop B and test it directly in A without passing through the usual process of publishing a version to npmjs.com .

So if for example, A depends on B, you can go into A and type `yarn link ../B`. The `package.json` of A will be modified to point to the local directory. Be very careful not to commit this (unless you know what you are doing).

Together with hot-swap (watch mode, dev mode,... whatever you call it), it allows easilly developing B while testing it directly in A before publishing.

This can be undone with `yarn unlink ../B`.

### How to use it in the root project

When using either [Clone everything and run everything](#clone-everything-and-run-everything) or [Clone everything but run partially](#clone-everything-but-run-partially), the root `package.json` contains an NPM script `yarn linkAll` that links all the dependent projects to all its dependencies (reversed with `yarn unlinkAll`).

So:
* Initialize the project as usual (clone, `yarn initAll`).
* Link all dependencies with `yarn linkAll`.
* Launch development mode with `yarn dev`.
* When finished, unlink the dependencies with `yarn unlinkAll`.

### How to use it in standalone clones

When using the [Clone standalone](#clone-standalone) mode, you need to do `yarn link` in the dependency yourself, and then do `yarn link <dependency>` in the dependent.

When using the containerized development environment, some care is needed, though. The reason is: for the `yarn link` mechanism to work, both the dependent and the dependency must be in the same filesystem. So in order to use it, we need to do the following:

* When clone-open the repository, choose the same docker volume in both projects. This is the default, anyway (`vsc-remote-containers`).
* Open both projects in VSCode.
* In the VSCode of the dependent project:
    * Go back to the directory of the dependent project.
    * Run `yarn link </path/to/dependency>`.
* In the VSCode of the dependency project:
    * Run `yarn dev`.
* In the VSCode of the dependent project:
    * Run `yarn dev`.

# FAQ

* Can I replicate this architecture for React/Next or other frameworks?

I have not tested, but most probably yes. Nothing in the concept here is specific for Vue/Nuxt (the details are, of course, you would need to addapt them yourself).

* Can I mix microfrontends in Vue/Nuxt with microfrontends in React/Next (or other frameworks)?

Yes, but probably it will be harder than you expect. The router, the login and the shared libraries will probably work flawlessly; but sharing the graphical components between the two frameworks will be a different beast. I have thought of several options, but all of them have problems.

The first option is creating the shared graphical components using WebComponents (for example, using Stencil), so they can be used in any framework. They work out-of-the-box for client-side (browser) rendering, but for SSR (Nuxt/Next) they will not. I still have not solved this. SSR was a design constraint for me, so I couldn't use this idea.

The second option is to indeed create two component libraries: one for Vue, another one for React, and take as much as possible the common logic to pure JS shared libraries. But this is more a give-up hackaround.

The sharing of the state through the store will be a challenge itsef: with a bit of luck, you can reuse the data in the Local Storage between the two frameworks... but I have not tried.

* Can I replicate this architecture for Nuxt2/Vue2?

I have not tested, but most probably yes. Nothing in the concept here is specific for Vue3/Nuxt3 (the details are, of course, you would need to addapt them yourself).

* Can I mix microfrontends in Vue3/Nuxt3 with microfrontends in Vue2/Nuxt2?

Kind of.

This is a particulr case of the "mix frameworks" above, with a couple of advantages making it easier (because Vue2 and Vue3 share the same philosophy and provide some interoperability):
* Sharing the state through `pinia-plugin-persist` should be doable if both use `pinia`. There is also `vuex-persistedstate` but as of the moment of writing this line, it looks abandoned.
* Sharing components should be doable, maybe using compatibility mode or Vue 2.7.

...but I have not tested it.

# TODO

* Use `store-library-pinia` in compiled mode.
* Remove workaround for https://github.com/nuxt/nuxt/issues/27447
* Demonstrate storing state in cookies
